package OBA;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class oba 
{
	private List records;
	private String uuid;
	@JsonProperty("vehicle-agency-id")
	private int vehicle_agency_id;
	@JsonProperty("time-reported")
	private Date time_reported;
	@JsonProperty("time-received")
	private Date time_received;
	@JsonProperty("archive-time-received")
	private Date archive_time_received;
	@JsonProperty("operator-id-designator")
	private int operator_id_designator;
	@JsonProperty("route-id-designator")
	private int route_id_designator;
	@JsonProperty("run-id-designator")
	private int run_id_designator;
	@JsonProperty("dest-sign-code")
	private int dest_sign_code;
	private double latitude;
	private double longitude;
	private double speed;
	@JsonProperty("direction-deg")
	private double direction_deg;
	@JsonProperty("agency-id")
	private String agency_id;
	@JsonProperty("vehicle-id")
	private int vehicle_id;
	@JsonProperty("depot-id")
	private String depot_id;
	@JsonProperty("service-date")
	private Date service_date;
	@JsonProperty("inferred-latitude")
	private double inferred_latitude;
	@JsonProperty("inferred-longitude")
	private double inferred_longitude;
	@JsonProperty("inferred-phase")
	private String inferred_phase;
	@JsonProperty("inferred-status")
	private String inferred_status;
	@JsonProperty("inference-is-formal")
	private String inference_is_formal;
	private String status;
	
	
	public List getrecords() 
	{
		return records;
	} 

	public void setrecords(List records) 
	{
		this.records = records;
	}
	
	public String getuuid() 
	{
		return uuid;
	} 

	public void setuuid(String uuid) 
	{
		this.uuid = uuid;
	}
	
	public int getvehicle_agency_id() 
	{
		return vehicle_agency_id;
	}

	public void setvehicle_agency_id(int vehicle_agency_id) 
	{
		this.vehicle_agency_id = vehicle_agency_id;
	}
	
	public Date gettime_reported() 
	{
		return time_reported;
	}

	public void settime_reported(Date time_reported) 
	{
		this.time_reported = time_reported;
	}
	
	public Date gettime_received() 
	{
		return time_reported;
	}

	public void settime_received(Date time_received) 
	{
		this.time_received = time_received;
	}
	
	public Date getarchive_time_received() 
	{
		return archive_time_received;
	}

	public void setarchive_time_received(Date archive_time_received) 
	{
		this.archive_time_received = archive_time_received;
	}
	
	public int getoperator_id_designator() 
	{
		return operator_id_designator;
	}

	public void setoperator_id_designator(int operator_id_designator) 
	{
		this.operator_id_designator = operator_id_designator;
	}
	
	public int getroute_id_designator() 
	{
		return route_id_designator;
	}

	public void setroute_id_designator(int route_id_designator) 
	{
		this.route_id_designator = route_id_designator;
	}
	
	public int getdest_sign_code() 
	{
		return dest_sign_code;
	}

	public void setdest_sign_code(int dest_sign_code) 
	{
		this.dest_sign_code = dest_sign_code;
	}
	
	public double getlatitude() 
	{
		return latitude;
	}

	public void setlatitude(double latitude) 
	{
		this.latitude = latitude;
	}
	
	public double getlongitude() 
	{
		return longitude;
	}

	public void setlongitude(double longitude) 
	{
		this.longitude = longitude;
	}
	
	public double getspeed() 
	{
		return speed;
	}

	public void setspeed(double speed) 
	{
		this.speed = speed;
	}
	
	public double direction_deg() 
	{
		return direction_deg;
	}

	public void direction_deg(double direction_deg) 
	{
		this.direction_deg = direction_deg;
	}
	
	public String agency_id() 
	{
		return agency_id;
	}

	public void agency_id(String agency_id) 
	{
		this.agency_id = agency_id;
	}
	
	public int vehicle_id() 
	{
		return vehicle_id;
	}

	public void vehicle_id(int vehicle_id) 
	{
		this.vehicle_id = vehicle_id;
	}
	
	public String depot_id() 
	{
		return depot_id;
	}

	public void depot_id(String depot_id) 
	{
		this.depot_id = depot_id;
	}
	
	public Date service_date() 
	{
		return service_date;
	}

	public void service_date(Date service_date) 
	{
		this.service_date = service_date;
	}
	
	public double inferred_latitude() 
	{
		return inferred_latitude;
	}

	public void inferred_latitude(int inferred_latitude) 
	{
		this.inferred_latitude = inferred_latitude;
	}
	
	public double inferred_longitude() 
	{
		return inferred_longitude;
	}

	public void inferred_longitude(int inferred_longitude) 
	{
		this.inferred_longitude = inferred_longitude;
	}
	
	public String inferred_phase() 
	{
		return inferred_phase;
	}

	public void inferred_phase(String inferred_phase) 
	{
		this.inferred_phase = inferred_phase;
	}
	
	public String inferred_status() 
	{
		return inferred_status;
	}

	public void inferred_status(String inferred_status) 
	{
		this.inferred_status = inferred_status;
	}
	
	public String inference_is_formal() 
	{
		return inference_is_formal;
	}

	public void inference_is_formal(String inference_is_formal) 
	{
		this.inference_is_formal = inference_is_formal;
	}
	
	public String getstatus() 
	{
		return status;
	}

	public void setstatus(String status) 
	{
		this.status = status;
	}
		
@Override
public String toString() 
{
  return "OBA BUS: [records =" + records + "], [status =" + status + "]";
}
}

